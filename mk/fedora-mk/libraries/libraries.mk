#
#       Searduino
#                                                                   
# Basically a couple of Makefile wrapping the Ardunio
# C and C++ files
#                                                                   
#  Copyright (C) 2012 Henrik Sandklef      
#                                                                   
# This program is free software; you can redistribute it and/or     
# modify it under the terms of the GNU General Public License       
# as published by the Free Software Foundation; either version 3    
# of the License, or any later version.                             
#                                                                   
#                                                                   
# This program is distributed in the hope that it will be useful,   
# but WITHOUT ANY WARRANTY; without even the implied warranty of    
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the     
# GNU General Public License for more details.                      
#                                                                   
# You should have received a copy of the GNU General Public License 
# along with this program; if not, write to the Free Software       
# Foundation, Inc., 51 Franklin Street, Boston,            
# MA  02110-1301, USA.                                              
#
#
SEARDUINO_PATH=../..
FEDORA_BUILD=true
FEDORASOURCES=true

ARDUINO_SOURCE_PATH=/usr/share/arduino/hardware/arduino/avr/cores/arduino/
ARDUINO_PATH=/usr/share/arduino/
ARDUINO_SHORT_PATH=/usr/share/arduino/
ARDUINO_LIBS_SOURCE_PATH=/usr/share/arduino/libraries



SRC_C= \
  $(ARDUINO_SHORT_PATH)/hardware/arduino/avr/libraries/Wire/src/utility/twi.c \
  $(ARDUINO_SHORT_PATH)/libraries/TFT/src/utility/glcdfont.c 


SRC_CXX= \
  $(ARDUINO_SHORT_PATH)/hardware/arduino/avr/cores/arduino/abi.cpp \
  $(ARDUINO_SHORT_PATH)/hardware/arduino/avr/cores/arduino/HardwareSerial0.cpp \
  $(ARDUINO_SHORT_PATH)/hardware/arduino/avr/cores/arduino/USBCore.cpp \
  $(ARDUINO_SHORT_PATH)/hardware/arduino/avr/cores/arduino/CDC.cpp \
  $(ARDUINO_SHORT_PATH)/hardware/arduino/avr/cores/arduino/HardwareSerial1.cpp \
  $(ARDUINO_SHORT_PATH)/hardware/arduino/avr/cores/arduino/Stream.cpp \
  $(ARDUINO_SHORT_PATH)/hardware/arduino/avr/cores/arduino/new.cpp \
  $(ARDUINO_SHORT_PATH)/hardware/arduino/avr/cores/arduino/HardwareSerial3.cpp \
  $(ARDUINO_SHORT_PATH)/hardware/arduino/avr/cores/arduino/WMath.cpp \
  $(ARDUINO_SHORT_PATH)/hardware/arduino/avr/cores/arduino/HardwareSerial.cpp \
  $(ARDUINO_SHORT_PATH)/hardware/arduino/avr/cores/arduino/HardwareSerial2.cpp \
  $(ARDUINO_SHORT_PATH)/hardware/arduino/avr/cores/arduino/Tone.cpp \
  $(ARDUINO_SHORT_PATH)/hardware/arduino/avr/cores/arduino/Print.cpp \
  $(ARDUINO_SHORT_PATH)/hardware/arduino/avr/cores/arduino/WString.cpp \
  $(ARDUINO_SHORT_PATH)/hardware/arduino/avr/cores/arduino/PluggableUSB.cpp \
  $(ARDUINO_SHORT_PATH)/hardware/arduino/avr/cores/arduino/IPAddress.cpp \
  $(ARDUINO_SHORT_PATH)/hardware/arduino/avr/cores/arduino/main.cpp \
  $(ARDUINO_SHORT_PATH)/hardware/arduino/avr/libraries/SPI/src/SPI.cpp \
  $(ARDUINO_SHORT_PATH)/hardware/arduino/avr/libraries/SoftwareSerial/src/SoftwareSerial.cpp \
  $(ARDUINO_SHORT_PATH)/hardware/arduino/avr/libraries/Wire/src/Wire.cpp \
  $(ARDUINO_SHORT_PATH)/hardware/arduino/avr/libraries/HID/src/HID.cpp \
  $(ARDUINO_SHORT_PATH)/libraries/Stepper/src/Stepper.cpp \
  $(ARDUINO_SHORT_PATH)/libraries/TFT/src/utility/Adafruit_GFX.cpp \
  $(ARDUINO_SHORT_PATH)/libraries/TFT/src/utility/Adafruit_ST7735.cpp \
  $(ARDUINO_SHORT_PATH)/libraries/TFT/src/TFT.cpp \
  $(ARDUINO_SHORT_PATH)/libraries/Ethernet/src/EthernetServer.cpp \
  $(ARDUINO_SHORT_PATH)/libraries/Ethernet/src/Dns.cpp \
  $(ARDUINO_SHORT_PATH)/libraries/Ethernet/src/EthernetUdp.cpp \
  $(ARDUINO_SHORT_PATH)/libraries/Ethernet/src/EthernetClient.cpp \
  $(ARDUINO_SHORT_PATH)/libraries/Ethernet/src/Ethernet.cpp \
  $(ARDUINO_SHORT_PATH)/libraries/Ethernet/src/Dhcp.cpp \
  $(ARDUINO_SHORT_PATH)/libraries/Ethernet/src/utility/w5100.cpp \
  $(ARDUINO_SHORT_PATH)/libraries/Ethernet/src/utility/socket.cpp 

#	$(ARDUINO_LIBS_SOURCE_PATH)/LiquidCrystal/LiquidCrystal.cpp \

#./Firmata/Firmata.cpp

LIB_PATH=$(SEARDUINO_PATH)/arduino-sources/libraries/libs/$(BOARD)/
LIB=$(LIB_PATH)/liblibraries.a



SEARDUINO_MK=$(SEARDUINO_PATH)/mk/searduino-vcs.mk

lib: $(LIB)
$(LIB): $(OBJ_C)  $(OBJ_CXX) 

include $(SEARDUINO_MK)
include $(SEARDUINO_FUNC)



objs: $(OBJ_C) $(OBJ_CXX)
	-ls -al $(OBJ_C)
	-ls -al $(OBJ_CXX)

due: ARDUINO=due
due: 
	make lib

uno: ARDUINO=uno
uno: 
	make lib

mega: ARDUINO=mega
mega: 
	make lib

mega2560: ARDUINO=mega2560
mega2560:
	make lib

leonardo: ARDUINO=leonardo
leonardo:
	make lib

