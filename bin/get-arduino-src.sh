#!/bin/bash

#
# Parse
#
if [ "$1" = "--avrtools-copy" ]
then
    AVRTOOLS_COPY=true
fi


if [ "$SEARD_OS" != "" ]
then
    MY_OS=$SEARD_OS
    OS_EXTRA_DIR="arduino-1.0.5"
elif [ "$1" = "--os" ]
then
    MY_OS=$2
    OS_EXTRA_DIR="arduino-1.0.5"
else
    MY_OS=$(uname -s )
    OS_EXTRA_DIR="arduino-1.0.5"
fi
OS_EXTRA_DIR="Arduino-1.8.5"
#https://github.com/arduino/Arduino/releases/download/1.8.5/Arduino-1.8.5.tar.xz
if [ "$MY_OS" = "Linux" ]
then
    if [ "$(uname -m | grep -c _64)" != "0" ] 
	then
	ARD_BASE=http://arduino.googlecode.com/files/
	ARD_FILE=arduino-1.0.5-linux64.tgz
	ARD_URL=$ARD_BASE/$ARD_FILE
        ARD_BASE=https://github.com/arduino/Arduino/releases/download/1.8.5/
        ARD_FILE=Arduino-1.8.5.tar.xz
	ARD_URL=$ARD_BASE/$ARD_FILE
    else
	ARD_BASE=http://arduino.googlecode.com/files/
	ARD_FILE=arduino-1.0.5-linux32.tgz
	ARD_URL=$ARD_BASE/$ARD_FILE
    fi
elif [ "${MY_OS:0:5}" = "CYGWI" ]
then
	ARD_BASE=http://arduino.googlecode.com/files/
	ARD_FILE=arduino-1.0.5-r2-windows.zip
	ARD_URL=$ARD_BASE/$ARD_FILE
	OS_EXTRA_DIR="arduino-1.0.5-r2"
elif [ "${MY_OS:0:6}" = "Darwin" ]
then
	ARD_BASE=http://arduino.googlecode.com/files/
	ARD_FILE=arduino-1.0.5-macosx.zip
	ARD_URL=$ARD_BASE/$ARD_FILE
	OS_EXTRA_DIR="Arduino.app/Contents/Resources/Java"
#OS_EXTRA_DIR="arduino-1.0.5"
else
    echo "Currently no support for non GNU/Linux platforms"
    echo "Contact the searduino team"
    exit 1
fi


exit_on_failure()
{
    RET=$1
    shift
    COMM="$*"
    if [ "$RET" != "0" ]
	then
	echo "***************************"
	echo "****  ERROR in $0:  $COMM"
	echo "***************************"
	exit $RET
    fi
}

exec_comm()
{
    echo "$*"
    $*
    exit_on_failure "$?" "$*"

#    echo "Finished: $*"
#    sleep 2
}




get_sources()
{
    mkdir -p download-tmp

    if [ "$ARDUINO_SOURCE" != "" ]
    then
	ARD_FILE=$ARDUINO_SOURCE
	exec_comm cp   $ARD_FILE download-tmp/
    else
	if [ ! -f $ARD_FILE ] && [ ! -f download-tmp/$ARD_FILE ]
	   then
	       if [ ! -f $ARD_FILE ]
	       then
		   #	exec_comm rm -f $ARD_FILE
		   wget $ARD_URL
	       fi
	       exec_comm mv   $ARD_FILE download-tmp
	fi
    fi


}

unpack_sources()
{
    exec_comm cd download-tmp
    if [ "$MY_OS" = "Linux" ]
    then
	exec_comm tar xJvf $ARD_FILE
    elif [ "${MY_OS:0:5}" = "CYGWI" ]
    then
	exec_comm unzip -u $ARD_FILE
    elif [ "${MY_OS:0:6}" = "Darwin" ]
    then
	exec_comm unzip -u $ARD_FILE
    else
	echo "Currently no support for non GNU/Linux platforms"
	echo "Contact the searduino team"
	exit 1
    fi
    exec_comm cd ..
}

get_lib()
{
    D_FILE=$1
    D_URL=$2
    D_DIR=$3
    D_NAME=$4

    if test -f libraries/${D_FILE} ;
    then
      echo "Not downloading $D_FILE)"
    else
      echo "Downloading $D_FILE ($D_URL)"

      cd arduino-sources/libraries
    
      wget $D_URL
      exit_on_failure $? "wget $D_URL"

      tar zxvf $D_FILE
      exit_on_failure $? "tar zxvf $D_FILE"

      mv  $D_DIR $D_NAME

      cd -
    fi
}


setup_sources()
{
    mkdir -p arduino-sources
    mkdir -p arduino-sources/core
    mkdir -p arduino-sources/variants
    mkdir -p ard-ex
    
    exec_comm cp -r download-tmp/${OS_EXTRA_DIR}/hardware/arduino/avr/cores/arduino/* arduino-sources/core
    exec_comm cp -r download-tmp/${OS_EXTRA_DIR}/hardware/arduino/avr/variants/* arduino-sources/variants/
    echo " *********************** COPY: download-tmp/${OS_EXTRA_DIR}/libraries"
    exec_comm cp -r download-tmp/${OS_EXTRA_DIR}/hardware/arduino/avr/libraries arduino-sources/
    exec_comm cp -r download-tmp/${OS_EXTRA_DIR}/libraries arduino-sources/
    exec_comm cp -r download-tmp/${OS_EXTRA_DIR}/build/shared/examples/* ard-ex/

    if [ "$AVRTOOLS_COPY" = "true" ]
    then
        mkdir -p avr-tools-copy/
        cp -r download-tmp/${OS_EXTRA_DIR}/hardware/tools/* avr-tools-copy/
    fi

    get_lib 1.0.1.tar.gz https://github.com/arduino-libraries/Mouse/archive/1.0.1.tar.gz Mouse-1.0.1         Mouse 
    get_lib 1.0.7.tar.gz https://github.com/arduino-libraries/LiquidCrystal/archive/1.0.7.tar.gz     LiquidCrystal-1.0.7 LiquidCrystal

}


create_mk()
{
    exec_comm cp mk/arduino-sources/Makefile.*                arduino-sources/
    exec_comm cp mk/libraries/Makefile.*                      arduino-sources/libraries/
##    exec_comm cp mk/arduino-sources/Makefile                  arduino-sources/
    exec_comm cp mk/arduino-sources/arduino-sources.mk        arduino-sources/
    exec_comm cp mk/libraries/libraries.mk                    arduino-sources/libraries/
}


if [ -f arduino-sources/core/WMath.cpp ]
then
    echo "Arduino code already downloaded, skipping download and unpack"
else
    get_sources
fi

unpack_sources
setup_sources
create_mk

exit 0
